organization in ThisBuild := "zw.co.alfredsamanga"
version in ThisBuild := "1.0-SNAPSHOT"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.12.8"

lazy val `lagom-tutorial` = (project in file("."))
  .aggregate(`product-api`, `product-impl`)

lazy val `product-api` = (project in file("product-api"))
  .settings(common)
  .settings(
    libraryDependencies ++= Seq(
      lagomJavadslApi,
      lombok
    )
  )

lazy val `product-impl` = (project in file("product-impl"))
  .enablePlugins(LagomJava)
  .settings(common)
  .settings(
    libraryDependencies ++= Seq(
      lagomJavadslPersistenceJdbc,
      lagomJavadslPersistenceJpa,
      lagomJavadslPersistenceCassandra,
      lagomJavadslKafkaBroker,
      lagomLogback,
      lagomJavadslTestKit,
      lombok
    )
      ++ BuildTarget.hibernatePersistenceDependencies
      ++ BuildTarget.serviceLocationSettings
      ++ BuildTarget.otherCommonSettings
  )
  .settings(lagomForkedTestSettings)
  .dependsOn(`product-api`)

val lombok = "org.projectlombok" % "lombok" % "1.18.8"

lagomCassandraEnabled in ThisBuild := false
lagomKafkaEnabled in ThisBuild := false

def common = Seq(
  javacOptions in Compile += "-parameters"
)
