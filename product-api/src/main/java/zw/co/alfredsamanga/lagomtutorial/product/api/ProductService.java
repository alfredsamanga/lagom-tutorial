package zw.co.alfredsamanga.lagomtutorial.product.api;

/**
 * Created by alfred on 02 July 2019
 */
import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.Method;

import static com.lightbend.lagom.javadsl.api.Service.*;

public interface ProductService extends Service {
    ServiceCall<NotUsed, String> test();

    @Override
    default Descriptor descriptor() {
        return named("products").withCalls(
                restCall(Method.GET, "/api/products/test", this::test)
        ).withAutoAcl(true);
    }
}
