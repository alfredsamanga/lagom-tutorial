package zw.co.alfredsamanga.lagomtutorial.product.repository.iface;

import zw.co.alfredsamanga.lagomtutorial.product.model.Product;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletionStage;

/**
 * Created by alfred on 02 July 2019
 */

public interface ProductRepository {
    CompletionStage<List<Product>> getProducts();
    CompletionStage<List<Product>> getProductsBySupplierId(UUID productId);
    CompletionStage<List<Product>> searchProducts(String search);
}
