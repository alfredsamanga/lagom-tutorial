package zw.co.alfredsamanga.lagomtutorial.product.repository.iface;

import zw.co.alfredsamanga.lagomtutorial.product.model.Product;
import zw.co.alfredsamanga.lagomtutorial.product.model.ProductSupplier;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletionStage;

/**
 * Created by alfred on 02 July 2019
 */

public interface ProductSupplierRepository {
    CompletionStage<List<ProductSupplier>> getProductSuppliers();
    CompletionStage<List<ProductSupplier>> getProductSuppliersBySupplierId(UUID productId);
    CompletionStage<List<ProductSupplier>> searchProductSuppliers(String search);
}
