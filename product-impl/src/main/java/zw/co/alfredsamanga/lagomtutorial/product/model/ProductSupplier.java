package zw.co.alfredsamanga.lagomtutorial.product.model;

/**
 * Created by alfred on 02 July 2019
 */

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity
@Indexed
@Table(name = "product_supplier")
@NamedQueries({
        @NamedQuery(name="ProductSupplier.getAll", query="SELECT ps FROM Product ps ORDER BY ps.dateCreated DESC")
})
public class ProductSupplier {
    @Id
    private UUID id;

    @CreationTimestamp
    private OffsetDateTime dateCreated;

    @UpdateTimestamp
    private OffsetDateTime lastUpdated;

    private UUID supplierId;

    @ManyToOne(targetEntity = Product.class)
    private Product product;
}
