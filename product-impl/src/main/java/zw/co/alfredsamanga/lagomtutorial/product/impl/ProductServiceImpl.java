package zw.co.alfredsamanga.lagomtutorial.product.impl;

/**
 * Created by alfred on 02 July 2019
 */
import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import zw.co.alfredsamanga.lagomtutorial.product.api.ProductService;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class ProductServiceImpl implements ProductService {
    @Override
    public ServiceCall<NotUsed, String> test() {
        return notUsed -> supplyAsync(() -> "Product service is up.");
    }
}
