package zw.co.alfredsamanga.lagomtutorial.product.model;

/**
 * Created by alfred on 02 July 2019
 */

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity
@Indexed
@Table(name = "product")
@NamedQueries({
        @NamedQuery(name="Product.getAll", query="SELECT p FROM Product p ORDER BY p.dateCreated DESC")
})
public class Product {
    @Id
    private UUID id;

    @CreationTimestamp
    private OffsetDateTime dateCreated;

    @UpdateTimestamp
    private OffsetDateTime lastUpdated;

    private String name;

    private String description;

    @OneToMany(mappedBy="product", fetch = FetchType.LAZY)
    private Set<ProductSupplier> productSuppliers;
}
