package zw.co.alfredsamanga.lagomtutorial.product.impl;

/**
 * Created by alfred on 02 July 2019
 */
import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;
import zw.co.alfredsamanga.lagomtutorial.product.api.ProductService;

public class ProductServiceModule extends AbstractModule implements ServiceGuiceSupport {
    @Override
    protected void configure() {
        bindService(ProductService.class, ProductServiceImpl.class);
    }
}
