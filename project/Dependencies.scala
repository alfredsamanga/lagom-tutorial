import com.lightbend.lagom.sbt.LagomImport.lagomJavadslJackson
import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport._
import com.typesafe.sbt.packager.docker.{Cmd, ExecCmd}
import play.sbt.PlayImport.javaJpa
import sbt._

object Version {
  val hibernateVersion = "5.2.11.Final"
  val hibernateSearchVersion = "5.8.2.Final"
  val postgresDriverVersion = "42.2.2"
  val hibernateCoreVersion = "5.4.1.Final"
  val mapStructVersion = "1.3.0.Final"
  val akkaDiscoveryServiceLocatorVersion = "1.0.0"
  val apacheCommonsVersion = "3.0"
  val openCsvVersion = "4.6"
  val jsonSimpleVersion = "1.1.1"
}

object CommonDependencies {
  val hibernatePersistence = Seq(
    lagomJavadslJackson,
    javaJpa,
    "org.hibernate" % "hibernate-ehcache" % Version.hibernateVersion,
    "org.hibernate" % "hibernate-search-orm" % Version.hibernateSearchVersion,
    "org.hibernate" % "hibernate-search-elasticsearch" % Version.hibernateSearchVersion,
    "org.postgresql" % "postgresql" % Version.postgresDriverVersion,
    "org.hibernate" % "hibernate-core" % Version.hibernateCoreVersion
  )

  val frontEndCommon = Seq(
    "org.ocpsoft.prettytime" % "prettytime" % "3.2.7.Final",
    "org.webjars" % "foundation" % "6.2.3",
    "org.webjars" % "foundation-icon-fonts" % "d596a3cfb3"
  )

  val gatlingTest = Seq(
    "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.3.0" % "test",
    "io.gatling" % "gatling-test-framework" % "2.3.0" % "test"
  )

  val serviceLocation = Seq(
    "com.lightbend.lagom" %% "lagom-scaladsl-akka-discovery-service-locator" % Version.akkaDiscoveryServiceLocatorVersion
  )

  val otherCommons = Seq(
    "org.apache.commons" % "commons-lang3" % Version.apacheCommonsVersion,
    "org.mapstruct" % "mapstruct" % Version.mapStructVersion,
    "org.mapstruct" % "mapstruct-processor" % Version.mapStructVersion,
    "com.opencsv" % "opencsv" % Version.openCsvVersion,
    "com.googlecode.json-simple" % "json-simple" % Version.jsonSimpleVersion
  )

  val dockerBuildSettings = Seq(
    dockerRepository := Some("lagomtutorial"),
    dockerCommands :=
      dockerCommands.value.flatMap {
        case cmd@Cmd("FROM", _) => List(cmd, Cmd("RUN", "apk add --no-cache bash"), Cmd("RUN", "apk --update add fontconfig ttf-dejavu"), Cmd("RUN", "apk add --update tzdata"), Cmd("ENV", "TZ Africa/Harare"))
        case ExecCmd("ENTRYPOINT", args@_*) => Seq(Cmd("ENTRYPOINT", args.mkString(" ")))
        case v => Seq(v)
      }
  )
}
