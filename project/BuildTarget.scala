import sbt._

object BuildTarget {
 
   val hibernatePersistenceDependencies: Seq[ModuleID] = CommonDependencies.hibernatePersistence
  
  val dockerBuildSettings = CommonDependencies.dockerBuildSettings
  
  val frontEndCommonSettings = CommonDependencies.frontEndCommon
  
  val serviceLocationSettings = CommonDependencies.serviceLocation
  
  val otherCommonSettings = CommonDependencies.otherCommons
}